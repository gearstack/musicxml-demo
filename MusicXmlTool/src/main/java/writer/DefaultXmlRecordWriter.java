package writer;

import model.MusicRecord;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

/**
 * This class is an implementation of the RecordWriter interface for XML-documents.
 */
public class DefaultXmlRecordWriter implements RecordWriter{

    @Override
    public void writeDocument(Collection<MusicRecord> records, Writer writer) throws IOException {

        // first construct a JDOM tree from the collection of MusicRecords
        Element root = new Element("matchingReleases");

        for (MusicRecord record: records) {

            Element recElement = new Element("release");

            Element name = new Element("name");
            name.addContent(record.getName());

            Element trackCount = new Element("trackCount");
            trackCount.addContent(Integer.toString(record.getTrackCount()));

            recElement.addContent(name);
            recElement.addContent(trackCount);

            root.addContent(recElement);
        }

        Document recordDocument = new Document(root);

        // format and write XML
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.output(recordDocument, writer);
    }

    @Override
    public void writeFile(Collection<MusicRecord> records, File outFile) throws IOException {
        FileWriter fileWriter = new FileWriter(outFile);
        this.writeDocument(records, fileWriter);
    }

    @Override
    public void writeFile(Collection<MusicRecord> records, String filePath) throws IOException {
        FileWriter fileWriter = new FileWriter(filePath);
        this.writeDocument(records, fileWriter);
    }
}
