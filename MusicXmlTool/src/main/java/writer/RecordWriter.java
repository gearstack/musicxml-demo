package writer;

import model.MusicRecord;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

/**
 * This Interface specifies methods for writing MusicRecords out as a document.
 */
public interface RecordWriter {

    /**
     * Writes out a collection MusicRecords using a provided writer.
     *
     * @param records the collection of MusicRecords to be written
     * @param writer the writer to use
     * @throws IOException if the writer is unable to write
     */
    void writeDocument(Collection<MusicRecord> records, Writer writer) throws IOException;

    /**
     * Writes out a collection of MusicRecords to a file.
     *
     * @param records the collection of MusicRecords to be written
     * @param outFile the file to write to
     * @throws IOException if unable to write to the file
     */
    void writeFile(Collection<MusicRecord> records, File outFile) throws IOException;

    /**
     * Writes out a collection of MusicRecords to a file with given path.
     *
     * @param records the collection of MusicRecords to be written
     * @param filePath the path of the file to write to
     * @throws IOException if unable to write to the file
     */
    void writeFile(Collection<MusicRecord> records, String filePath) throws IOException;
}
