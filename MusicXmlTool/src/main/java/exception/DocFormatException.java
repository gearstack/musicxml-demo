package exception;

/**
 * This exception is thrown if an invalid document is encountered.
 */
public class DocFormatException extends Exception {

    public DocFormatException(String message) {
        super(message);
    }

    public DocFormatException(String message, Throwable cause){
        super(message, cause);
    }
}
