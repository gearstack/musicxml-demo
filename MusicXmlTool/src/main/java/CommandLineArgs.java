import com.beust.jcommander.Parameter;

/**
 * This class defines the arguments for the commandline interface.
 */
public class CommandLineArgs {

    @Parameter(names = {"--input", "-i"}, description = "input file path", required = true)
    String inputFilePath;

    @Parameter(names = {"--output", "-o"}, description = "output file path")
    String outputFilePath = "output.xml";
}
