package model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Plain old Java object representing a music record.
 */
public class MusicRecord {

    private String title;
    private String name;
    private String genre;
    private String label;
    private String formats;

    private Date releaseDate;

    private List<MusicTrack> trackList;


    public int getTrackCount() {
        return this.trackList.size();
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.mm.yyyy");
        return String.format(
                "%s; release: %s; tracks: %d",
                this.title,
                dateFormatter.format(this.releaseDate),
                this.getTrackCount()
        );
    }

    /*
    Getters & Setters
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getFormats() {
        return formats;
    }

    public void setFormats(String formats) {
        this.formats = formats;
    }

    public List<MusicTrack> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<MusicTrack> trackList) {
        this.trackList = trackList;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }
}
