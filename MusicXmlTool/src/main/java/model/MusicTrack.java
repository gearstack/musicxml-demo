package model;

/**
 * Plain old Java object representing a music track.
 */
public class MusicTrack {

    private String title;

    public MusicTrack() {
        // empty default constructor
    }

    public MusicTrack(String title) {
        this();
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

