import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import exception.DocFormatException;
import model.MusicRecord;
import parser.RecordParser;
import parser.DefaultXmlRecordParser;
import writer.RecordWriter;
import writer.DefaultXmlRecordWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class MusicXmlToolMain {

    final private static String appName = "MusicXMLTool";


    private static CommandLineArgs getCliArgs(String[] args) {

        // build commandline parser with provided arguments
        CommandLineArgs cliArgs = new CommandLineArgs();
        JCommander cliParser = JCommander.newBuilder().addObject(cliArgs).build();
        cliParser.setProgramName(appName);

        try {
            cliParser.parse(args);
        } catch (ParameterException e) {
            // required cli-args are missing -> print usage and terminate
            cliParser.usage();
            System.exit(0);
        }
        return cliArgs;
    }


    private static Collection<MusicRecord> parseInputFile(String filePath) {
        File xmlFile = new File(filePath);
        RecordParser parser = new DefaultXmlRecordParser();

        Collection<MusicRecord> records = null;
        try {
            records = parser.parseFile(xmlFile);
        } catch (DocFormatException e) {
            System.err.println(String.format("Invalid input file (%s)", e.getMessage()));
            System.exit(1);
        } catch (IOException e) {
            System.err.println(
                    String.format("Something went wrong while trying to read the input file (%s)", e.getMessage()));
            System.exit(1);
        } catch (ParseException e) {
            System.err.println(String.format("Unable to parse date string (%s)", e.getMessage()));
            System.exit(1);
        }
        return records;
    }


    private static Collection<MusicRecord> filterRecords(Collection<MusicRecord> records) {
        Date filterDate = null;
        try {
            filterDate = new SimpleDateFormat("dd.mm.yyyy").parse("01.01.2001");
        } catch (ParseException e) {
            // this should never happen without parsing external strings
            e.printStackTrace();
        }

        // the .filter method asks for a 'final' object
        final Date finalFilterDate = filterDate;
        List<MusicRecord> resultList = records.stream()
                .filter(r -> r.getTrackCount() > 10)
                .filter(r -> r.getReleaseDate().before(finalFilterDate))
                .collect(Collectors.toList());
        return resultList;
    }


    private static void writeOutputFile(Collection<MusicRecord> records, String outputPath) {
        RecordWriter writer = new DefaultXmlRecordWriter();

        // write to file and make sure it will be closed properly.
        try (FileWriter fw = new FileWriter(outputPath)) {
            writer.writeDocument(records, fw);
        } catch (IOException e) {
            System.err.println(
                    String.format("Something went wrong while writing to %s (%s)", outputPath, e.getMessage()));
        }
    }


    public static void main(String[] args){
        // parse cli-args
        CommandLineArgs cliArgs = MusicXmlToolMain.getCliArgs(args);

        // parse records from inputfile
        System.out.println(String.format("Reading '%s'", cliArgs.inputFilePath));
        Collection<MusicRecord> records = MusicXmlToolMain.parseInputFile(cliArgs.inputFilePath);

        // apply filters to collection
        System.out.println("Filtering records");
        Collection<MusicRecord> filteredRecords = MusicXmlToolMain.filterRecords(records);

        // write filtered collection to outputfile
        System.out.println(String.format("Writing results to '%s'", cliArgs.outputFilePath));
        MusicXmlToolMain.writeOutputFile(filteredRecords, cliArgs.outputFilePath);
        System.out.println("Done.");
    }
}
