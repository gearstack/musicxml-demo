package parser;

import exception.DocFormatException;
import model.MusicRecord;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Collection;

/**
 * This interface specifies methods for parsing MusicRecords from a document.
 */
public interface RecordParser {

    /***
     * Accepts a file as an input, parses it and returns a collection of MusicRecords.
     *
     * @param document the document to parse
     * @return a collection of MusicRecord objects
     * @throws DocFormatException if an invalid document format is encountered
     * @throws IOException if there is a problem reading the file
     * @throws ParseException if an unparsable expression is encountered
     */
    Collection<MusicRecord> parseFile(File document) throws DocFormatException, IOException, ParseException;

    /***
     * Accepts a file path as an input, opens the file, parses it and returns a collection of MusicRecords.
     *
     * @param fileName the name of the file to parse
     * @return a collection of MusicRecord objects
     * @throws DocFormatException if an invalid document format is encountered
     * @throws IOException if there is a problem reading the file
     * @throws ParseException if an unparsable expression is encountered
     */
    Collection<MusicRecord> parseFile(String fileName) throws DocFormatException, IOException, ParseException;
}
