package parser;

import exception.DocFormatException;
import model.MusicRecord;
import model.MusicTrack;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * This class implements the RecordParser inferface for the XML format that is used in this application.
 */
public class DefaultXmlRecordParser implements RecordParser {

    // set up a dateformat for parsing dates from strings
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.mm.dd");

    @Override
    public List<MusicRecord> parseFile(File xmlFile) throws DocFormatException, IOException, ParseException {
        List<MusicRecord> records = new LinkedList<MusicRecord>();

        // construct JDOM tree
        SAXBuilder jdomBuilder = new SAXBuilder();

        Document jdomDocument;
        try {
            jdomDocument = jdomBuilder.build(xmlFile);
        } catch (JDOMException e) {
            throw new DocFormatException("Invalid XML file format.", e);
        }

        // get root element and iterate over its children
        Element rootElement = jdomDocument.getRootElement();

        for (Element recordElement: rootElement.getChildren("record")) {

            // create new MusicRecord object and put in the plain string values
            MusicRecord rec = new MusicRecord();
            rec.setTitle(recordElement.getChildText("title"));
            rec.setName(recordElement.getChildText("name"));
            rec.setGenre(recordElement.getChildText("genre"));
            rec.setLabel(recordElement.getChildText("label"));
            rec.setFormats(recordElement.getChildText("formats"));

            // parse releasedate string to a java date object
            String releaseDateText = recordElement.getChildText("releasedate");
            rec.setReleaseDate(DefaultXmlRecordParser.dateFormat.parse(releaseDateText));

            // iterate over the tracklist, create new track objects and add them to the MusicRecord object
            Element trackListing = recordElement.getChild("tracklisting");
            List<MusicTrack> trackList = new LinkedList<MusicTrack>();
            for (Element track: trackListing.getChildren("track")) {
                trackList.add(new MusicTrack(track.getText()));
            }
            rec.setTrackList(trackList);

            // finally add the created object to the collection to return.
            records.add(rec);
        }
        return records;
    }

    @Override
    public List<MusicRecord> parseFile(String filePath) throws DocFormatException, IOException, ParseException {
        File xmlFile = new File(filePath);
        return this.parseFile(xmlFile);
    }
}
