README MusicXmlTool

Dieses Programm wurde im Rahmen eines Tests für Entwickler erstellt.
Gegeben ist dabei eine XML-Datei, die Informationen zu einer Liste von Musik-CDs enthält.
Die Software soll in der Lage sein, diese Liste nach festgelegten Kriterien zu filtern und
das Resultat in eine seperate XML-Datei mit spezifiziertem Format zu schreiben.
Zusätzlich soll das Programm so aufgebaut sein, dass es ohne große Modifikationen auch anders 
aufgebaute XML-Dateien, oder auch andere Formate verarbeiten kann.

Requirements
============

Java (>= 1.8)

externe libraries:

JDOM (2.0.6) - http://www.jdom.org/
JCommander (1.72) - http://jcommander.org/


Contents
========

Neben diesem Readme enthält das Archiv:

- worldofmusic.xml - vorgegebene XML-Datei mit zu verarbeitenden Infos
- MusicXmlTool.zip - enthält das komplette IntelliJ Projekt mit dem source code
- MusicXmlTool.jar - gebaute und gepackte Anwendung
    - Usage: java -jar MusicXmlTool.jar -i <inputfile> [-o <outputfile>]

Author
======

* Maximilian Rihlmann (m.rihlmann@gmx.de)